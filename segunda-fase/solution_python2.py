#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import time
import datetime

## Helpers

# lê um csv e salva cada linha em um Dict
def read_file(filename):
    with open(filename, 'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        headers = next(csvreader)
        return [ dict(zip(headers, row)) for row in csvreader ]

# escreve um array de Dicts em um CSV
def write_file(filename, rows):
    with open(filename, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile)
        header = rows[0].keys()
        csvwriter.writerow(header)
        for row in rows:
            csvwriter.writerow([row[field] for field in header])

# usa uma coluna para achar mais fácil qual linha contém esse valor
def create_index_unique(key, rows):
    return { row[key]: row for row in rows }

# usa uma coluna para achar mais fácil quais linhas contém esse valor
def create_index_multiple(key, rows):
    result = {}
    for row in rows:
        if row[key] not in result:
            result[row[key]] = []
        result[row[key]] += [row]
    return result

# transforma uma string que representa uma data em uma data
def parse_date(str):
    return datetime.datetime.strptime(str, '%Y-%m-%d %H:%M:%S')

# calcula a diferença em segundos entre duas datas em string
def date_diff(str1, str2):
    return (parse_date(str1)-parse_date(str2)).total_seconds()

# solução do primeiro exercício
# reputação dos fornecedores
def calcula_fornecedores_ordenados(clientes,
                                   transacoes,
                                   produtos_por_transacao,
                                   produtos,
                                   fornecedores):

    compras = filter(lambda x: x['tipo_transacao']=='compra', transacoes.values())
    compras_por_fornecedor = create_index_multiple('codigo_transacionado', compras)

    for id in fornecedores:
        if id not in compras_por_fornecedor:
            compras_por_fornecedor[id]=[]


    for id, fornecedor in fornecedores.items():
        fornecedor['compras']=0
        fornecedor['tempo_total']=0
        for compra in compras_por_fornecedor[fornecedor['codigo_fornecedor']]:
            if compra['data_entrega']!='':
                fornecedor['compras']+=1
                fornecedor['tempo_total']+=date_diff(compra['data_entrega'],compra['data_compra'])
        if fornecedor['tempo_total'] > 0:
            fornecedor['tempo_medio']=fornecedor['tempo_total']/fornecedor['compras']
        else:
            fornecedor['tempo_medio']=None

    fornecedores_ordenados = []
    total_fornecedores = len(fornecedores)
    keys = ['codigo_fornecedor', 'nome', 'reputacao']

    for id, fornecedor in fornecedores.items():
        if fornecedor['tempo_medio'] == None:
            fornecedor['reputacao'] = 0
        elif fornecedor['tempo_medio'] <= 48 * 60 * 60: # 48 horas
            fornecedor['reputacao'] = 5
        elif fornecedor['tempo_medio'] <= 5 * 24 * 60 * 60: # 5 dias
            fornecedor['reputacao'] = 4
        elif fornecedor['tempo_medio'] <= 15 * 24 * 60 * 60: # 15 dias
            fornecedor['reputacao'] = 3
        elif fornecedor['tempo_medio'] <= 30 * 24 * 60 * 60: # 30 dias
            fornecedor['reputacao'] = 2
        else:
            fornecedor['reputacao'] = 1
        fornecedores_ordenados += [{key: fornecedor[key] for key in keys}]

    return sorted(fornecedores_ordenados, key=lambda x: (-x['reputacao'], x['nome']))

# solução do segundo exercício
# estoque de produtos
def calcula_estoque_produtos(clientes,
                             transacoes,
                             produtos_por_transacao,
                             produtos,
                             fornecedores):
    for id, produto in produtos.items():
        produto['quantidade'] = 0

    for id, transacao in transacoes.items():
        for assoc in produtos_por_transacao[id]:
            if transacao['tipo_transacao']=='venda':
                produtos[assoc['codigo_produto']]['quantidade']-=int(assoc['quantidade'])
            elif transacao['data_entrega']!='':
                produtos[assoc['codigo_produto']]['quantidade']+=int(assoc['quantidade'])

    estoque_produtos = []
    keys = ['codigo_produto', 'nome', 'quantidade']

    for id, produto in produtos.items():
        estoque_produtos += [{key: produto[key] for key in keys}]

    return sorted(estoque_produtos, key=lambda p: p['nome'])


# solução do terceiro exercício
# cidades por total gasto
def calcula_cidades_ordenadas(clientes,
                              transacoes,
                              produtos_por_transacao,
                              produtos,
                              fornecedores):

    def cidade_do_cliente(cliente):
        return cliente['endereco'].split(',')[-2].strip()

    cidades = {}

    vendas = filter(lambda x: x['tipo_transacao']=='venda', transacoes.values())
    vendas_por_cliente = create_index_multiple('codigo_transacionado', vendas)

    for id in clientes:
        if id not in vendas_por_cliente:
            vendas_por_cliente[id]=[]

    for id, cliente in clientes.items():
        cidade = cidade_do_cliente(cliente)
        if cidade not in cidades:
            cidades[cidade] = 0
        for venda in vendas_por_cliente[id]:
            valor_venda = 0
            for assoc in produtos_por_transacao[venda['codigo_transacao']]:
                valor_venda += float(produtos[assoc['codigo_produto']]['valor_venda'])*float(assoc['quantidade'])
            if cliente['vip']=='true':
                valor_venda*=0.95
            cidades[cidade]+=valor_venda

    cidades_ordenadas = []
    for nome, total in sorted(cidades.items(), key=lambda pair: -pair[1]):
        cidades_ordenadas+=[{'nome_da_cidade': nome, 'total_em_reais': "%.2f" % total}]

    return cidades_ordenadas

# lê a entrada
def le_entrada():
    """
    # modo de usar a entrada lida dessa maneira
    # para iterar sobre todos os clientes use
    for id, cliente in clientes.items():
    # para encontrar o cliente cujo código é x use
    cliente = clientes[x]
    """
    diretorio = 'input-final/'
    clientes = create_index_unique('codigo_cliente', read_file(diretorio+'clientes.csv'))
    transacoes = create_index_unique('codigo_transacao', read_file(diretorio+'transacoes.csv'))
    produtos_por_transacao = create_index_multiple('codigo_transacao', read_file(diretorio+'transacao_produto.csv'))
    produtos = create_index_unique('codigo_produto', read_file(diretorio+'produtos.csv'))
    fornecedores = create_index_unique('codigo_fornecedor', read_file(diretorio+'fornecedores.csv'))

    return clientes, transacoes, produtos_por_transacao, produtos, fornecedores

# executa o programa
def calcula_tudo():
    entrada = le_entrada()
    diretorio = 'output-final/'
    write_file(diretorio+'fornecedores_ordenados.csv', calcula_fornecedores_ordenados(*entrada))
    write_file(diretorio+'estoque_produtos.csv', calcula_estoque_produtos(*entrada))
    write_file(diretorio+'cidades_ordenadas.csv', calcula_cidades_ordenadas(*entrada))

if __name__ == "__main__":
    calcula_tudo()
