require 'csv'

output_dir = 'output-final/'
gabarito_dir = 'gabarito-final/'

file_list = ["cidades_ordenadas.csv",
             "estoque_produtos.csv",
             "fornecedores_ordenados.csv"]


def compare_lines(output, gabarito)
  gabarito.all? do |field|
    output.member?(field)
  end
end

def compare_file(output, gabarito)
  out_file = CSV.open(output, "rb")
  gab_file = CSV.open(gabarito, "rb")
  out_file.readline # drop header
  gab_file.readline # drop header
  index = 1

  while true
    out_line = out_file.readline
    gab_line = gab_file.readline

    if out_line.nil? || gab_line.nil?
      out_file.close
      gab_file.close

      if out_line.nil? && gab_line.nil?
        return :ok
      elsif out_line.nil?
        return "output missing some lines"
      else
        return "output has extra lines"
      end
    end

    if !compare_lines(out_line, gab_line)
      out_file.close
      gab_file.close
      return "line #{index} differs"
    end

    index += 1
  end
end

file_list.each do |filename|
  comparison = compare_file(output_dir+filename, gabarito_dir+filename)
  if comparison == :ok
    puts "#{filename} OK!"
  else
    puts "#{filename} failed: #{comparison}."
  end
end
