#include <stdio.h>

/* 101 para usar indices ate 100 inclusive */
int corrida[101][101];
int sistema[101];
int piloto[101];

int main() {
  int g, p, s; /* corridas, pilotos e sistemas */
  int i, j, k; /* iteradores */
  int maior; /* maior pontuacao obtida */
  int tmp;
  while(1) {
    scanf(" %d %d", &g, &p);
    if (g==0 && p==0)
      break;
    for(i=1; i<=g; ++i)
      for(j=1; j<=p; ++j)
        scanf(" %d", &corrida[i][j]);
    scanf(" %d", &s);
    for(k=1; k<=s; ++k) {
      for(i=1; i<=p; ++i) {
        sistema[i]=0;
        piloto[i]=0;
      }
      maior=0;
      scanf(" %d", &tmp);
      for(i=1; i<=tmp; ++i)
        scanf(" %d", &sistema[i]);
      for(i=1; i<=g; ++i)
        for(j=1; j<=p; ++j)
          piloto[j]+=sistema[corrida[i][j]];
      tmp=0;
      for(j=1; j<=p; ++j)
        if (piloto[j]>maior)
          maior=piloto[j];
      for(j=1; j<=p; ++j)
        if (piloto[j]==maior) {
          if (tmp>0)
            printf(" ");
          printf("%d", j);
          ++tmp;
        }
      printf("\n");
    }
  }
  return 0;
}
