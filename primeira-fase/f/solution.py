# python3

def calcula_pontos(posicoes, sistema):
    total = 0
    for p in posicoes:
        if p < len(sistema):
            total+=sistema[p]
    return total

while True:
    g, p = [int(x) for x in input().split()]
    if g==0 and p==0:
        break
    corridas = []
    for i in range(g):
        corridas += [[int(x) for x in input().split()]]
    pilotos = []
    for i in range(p):
        pilotos += [[corridas[j][i] for j in range(g)]]

    sistemas = int(input())
    for s in range(sistemas):
        sistema = [int(x) for x in input().split()]
        pontos = [calcula_pontos(piloto, sistema) for piloto in pilotos]
        maior = max(pontos)
        print(*[i for i, p in enumerate(pontos, start=1) if p==maior])


