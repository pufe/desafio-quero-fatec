#include <stdio.h>

int main() {
  int teste=1;
  int total;
  int notas_50;
  int notas_10;
  int notas_5;
  int notas_1;
  while(1) {
    scanf(" %d", &total);
    if (total==0)
      break;
    notas_50 = total/50;
    total -= 50*notas_50;
    notas_10 = total/10;
    total -= 10*notas_10;
    notas_5 = total/5;
    total -= 5*notas_5;
    notas_1 = total;
    printf("Teste %d\n", teste);
    printf("%d %d %d %d\n", notas_50, notas_10, notas_5, notas_1);
    printf("\n");
    ++teste;
  }
  return 0;
}
