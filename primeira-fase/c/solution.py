# python3

total = int(input())
lista = [int(x) for x in input().split()]
divisores = [2, 3, 4, 5]

for d in divisores:
    contagem = len([x for x in lista if x%d==0])
    print(contagem, "Multiplo(s) de", d)
