#include <stdio.h>

int v[1000];

int main() {
  int n, d, i, c;
  scanf(" %d", &n);
  for(i=0; i<n; ++i)
    scanf(" %d", &v[i]);
  for(d=2; d<=5; ++d) {
    c=0;
    for(i=0; i<n; ++i)
      if (v[i]%d==0)
        ++c;
    printf("%d Multiplo(s) de %d\n", c, d);
  }
  return 0;
}
