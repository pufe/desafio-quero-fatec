#include <stdio.h>

int idades[11];

int main() {
  int num_testes, teste;
  int n, i;
  scanf(" %d", &num_testes);
  for(teste=1; teste<=num_testes; ++teste) {
    scanf(" %d", &n);
    for(i=0; i<n; ++i)
      scanf(" %d", &idades[i]);
    printf("Case %d: %d\n", teste, idades[n/2]);
  }
  return 0;
}

