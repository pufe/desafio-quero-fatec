#include <stdio.h>
#include <stdlib.h>

int v[100];

int cmp(const void* a, const void* b) {
  int int_a = *(int*)a;
  int int_b = *(int*)b;

  if (int_a>int_b)
    return -1;
  else if (int_a<int_b)
    return 1;
  else
    return 0;
}

int main() {
  int n, q, i, x;
  while(1) {
    if (scanf(" %d %d", &n, &q)!=2)
      break;
    for(i=0; i<n; ++i)
      scanf(" %d", &v[i]);
    qsort(v, n, sizeof(int), cmp);
    for(i=0; i<q; ++i) {
      scanf(" %d", &x);
      --x;
      printf("%d\n", v[x]);
    }
  }
  return 0;
}
