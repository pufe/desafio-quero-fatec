# python3

import sys

while True:
    line = sys.stdin.readline()
    if line=='':
        break
    n, q = [int(x) for x in line.split()]
    notas = [int(input()) for x in range(n)]
    notas.sort()
    notas.reverse()
    for i in range(q):
        posicao = int(input())
        print(notas[posicao-1])



